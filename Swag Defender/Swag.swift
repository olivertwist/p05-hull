//
//  swag.swift
//  Swag Defender
//
//  Created by oliver on 4/18/17.
//  Copyright © 2017 oliver. All rights reserved.
//

import Foundation
import SpriteKit


class Swag: SKSpriteNode {
    
    private var health: Int?
    public var ammo: Int?
    
    
    init() {
        let texture = SKTexture(imageNamed: "swag.png")
        super.init(texture: texture, color: SKColor.blue, size: CGSize(width: 160, height: 250))
        self.name = "swag"
        
        self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
        self.physicsBody?.categoryBitMask = SwagCategory
        self.physicsBody?.contactTestBitMask = EnemyCategory | AmmoCategory
        
        self.ammo = 10
        self.health = 10
    }
    
    
    func gotHit() -> Int {
        self.health! -= 1
        
        return self.health!
        
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    
    
}
