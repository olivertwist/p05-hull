//
//  Enemy.swift
//  Swag Defender
//
//  Created by oliver on 4/19/17.
//  Copyright © 2017 oliver. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit


class Enemy: SKSpriteNode {
    
    public var rocketBoost: SKEmitterNode?
    public var pinJoint: SKPhysicsJointFixed?
    
    
    
    init() {
        let texture = SKTexture(imageNamed: "brendun.png")
        super.init(texture: texture, color: SKColor.blue, size: CGSize(width: 100, height: 140))
        self.name = "Brendun"
        
        self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.velocity = CGVector(dx: 0, dy: -200)
        self.physicsBody?.categoryBitMask = EnemyCategory
        self.physicsBody?.contactTestBitMask = 0
        
        
        
        
        
        rocketBoost = SKEmitterNode( fileNamed:"fire.sks")
        self.rocketBoost?.physicsBody = SKPhysicsBody(circleOfRadius: 16)
        
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
