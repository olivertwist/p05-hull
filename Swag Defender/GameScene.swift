//
//  GameScene.swift
//  s
//
//  Created by oliver on 4/18/17.
//  Copyright © 2017 oliver. All rights reserved.
//

import SpriteKit
import GameplayKit



let SwagCategory: UInt32  = 0x1 << 1

let EnemyCategory: UInt32 = 0x1 << 2
let FloorCategory: UInt32  = 0x1 << 3
let BulletCategory: UInt32  = 0x1 << 4
let AmmoCategory: UInt32  = 0x1 << 5









extension CGVector {
    func speed() -> CGFloat {
        return sqrt(dx*dx+dy*dy)
    }
    func angle() -> CGFloat {
        return atan2(dy, dx)
    }
}

class GameScene: SKScene , SKPhysicsContactDelegate{
    
    private var swag : SKSpriteNode?
    private var floor : SKSpriteNode?
    private var healthBar : SKSpriteNode?
    private var healthBarSize : CGSize?
    private var ammoLabel: SKLabelNode?
    private var scoreLabel: SKLabelNode?
    private var tryAgain : SKSpriteNode?
    private var score: Int?
    
    

    private var enemies : NSMutableArray?
    
    override func didMove(to view: SKView) {
        
        loadSceneNodes()
        
        
        
    }
    
    
    func touchDown(atPoint pos : CGPoint) {

    }
    
    func touchMoved(toPoint pos : CGPoint) {

    }
    
    func touchUp(atPoint pos : CGPoint) {

    
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            let location = t.location(in: self)
           
            if self.childNode(withName: "tryAgain") != nil {
                if (tryAgain?.contains(location))! {
                    self.resetScene()
                }
            }
            else if !(floor?.contains(location))! &&  ((swag as? Swag)?.ammo)! > 0{
                self.shoot()
                (swag as? Swag)?.ammo! -= 1
                ammoLabel?.text = "AMMO: " + ((swag as? Swag)?.ammo!.description)!

                
            }
        }

    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            let location = t.location(in: self)
            if(floor?.contains(location))!{

                if((location.x > (swag?.position.x)! + (swag?.size.width)!/2.0) && (swag?.physicsBody?.velocity.dx)! < 300.0){
                    swag?.physicsBody?.applyImpulse(CGVector(dx: 20,dy: 0))

                }
                else if((location.x < (swag?.position.x)! - (swag?.size.width)!/2.0) && (swag?.physicsBody?.velocity.dx)! > -300.0){
                    
                    swag?.physicsBody?.applyImpulse(CGVector(dx: -20,dy: 0))
                    
                }
            }
        }

    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        for enemy in enemies! {
            if((enemy as! SKSpriteNode).position.y > (swag?.position.y)! + (swag?.size.height)!){
                if((enemy as! SKSpriteNode).position.x > (swag?.position.x)!){
                    (enemy as! SKSpriteNode).physicsBody?.applyImpulse(CGVector(dx: -2,dy: 0))
                
                }
                else if((enemy as! SKSpriteNode).position.x < (swag?.position.x)!){
                    (enemy as! SKSpriteNode).physicsBody?.applyImpulse(CGVector(dx: 2,dy: 0))
                
                }
                else{
                    (enemy as! SKSpriteNode).physicsBody?.velocity.dx = 0
                }
            
                
                
                (enemy as! Enemy).zRotation = ((enemy as! SKSpriteNode).physicsBody?.velocity.angle())! - CGFloat(M_PI_2)
                
                
            }
            else{
                (enemy as! Enemy).zRotation = 2*CGFloat(M_PI_2)
            }
        
        }

    
        
    }
    

    
    
    //MARK: Contact Delegate

    func didBegin(_ contact: SKPhysicsContact) {
        if contact.bodyA.node?.name == "swag" && contact.bodyB.node?.name == "Brendun" {
            let remainingHealth = (swag as! Swag).gotHit()
            
            if(remainingHealth == 1){
                healthBar?.color = SKColor.red
                healthBar?.size.width = (healthBarSize?.width)! * (CGFloat(remainingHealth)/10.0)
            }
            else if(remainingHealth == 0){
             
                healthBar?.color = SKColor.clear
                healthBar?.size = healthBarSize!
                self.gameOver()
            
            }
            else{
                
                healthBar?.size.width = (healthBarSize?.width)! * (CGFloat(remainingHealth)/10.0)
            
            }
            
                
            contact.bodyB.node?.removeFromParent()
            (contact.bodyB.node as? Enemy)?.rocketBoost?.removeFromParent()
        }
        else if contact.bodyA.node?.name == "floor" && contact.bodyB.node?.name == "Brendun"{
            contact.bodyB.node?.removeFromParent()
            (contact.bodyB.node as? Enemy)?.rocketBoost?.removeFromParent()

        }
        else if contact.bodyA.node?.name == "Brendun" && contact.bodyB.node?.name == "bullet"{
            contact.bodyB.node?.removeFromParent()
            contact.bodyA.node?.removeFromParent()
            (contact.bodyA.node as? Enemy)?.rocketBoost?.removeFromParent()
            score? += 10
            scoreLabel?.text = String(format: "SCORE: %d", arguments: [score!])


        }
        if contact.bodyA.node?.name == "swag" && contact.bodyB.node?.name == "ammoCrate" {

            
            
            contact.bodyB.node?.removeFromParent()
			
            (swag as? Swag)?.ammo! += 10
            ammoLabel?.text = "AMMO: "+((swag as? Swag)?.ammo!.description)!

        }
        
        
    }
    
    
    
    
    
    
    
    
    
    func loadSceneNodes(){
        score = 0
        enemies = NSMutableArray(capacity: 40)

        self.view?.showsPhysics = true
        physicsWorld.contactDelegate = self
        
        
        swag = Swag()
        self.addChild(swag!)

        floor = childNode(withName: "floor") as? SKSpriteNode
        healthBar = childNode(withName: "healthBar") as? SKSpriteNode
        healthBarSize = healthBar?.size
        
        ammoLabel = childNode(withName: "ammoLabel") as? SKLabelNode
        ammoLabel?.text = "AMMO: 10"        
        
        scoreLabel = childNode(withName: "scoreLabel") as? SKLabelNode
        scoreLabel?.text = String(format: "SCORE: %d", arguments: [score!])


        
        
        spawnEnemies()
        
        spawnAmmo()
    
        
    }
    

    
    
    
    
    func spawnAmmo(){
        let delay = SKAction.wait(forDuration: 15, withRange: 4)
        let spawn = SKAction.run {
            
            let ammoCrate = SKSpriteNode(color: SKColor.red, size: CGSize(width: 100, height: 100))
            ammoCrate.physicsBody = SKPhysicsBody(rectangleOf: ammoCrate.size)
            ammoCrate.physicsBody?.affectedByGravity = false
            ammoCrate.physicsBody?.allowsRotation = false
            
            
            ammoCrate.physicsBody?.velocity = CGVector(dx: 0, dy: -100)
            ammoCrate.physicsBody?.categoryBitMask = AmmoCategory
            ammoCrate.physicsBody?.contactTestBitMask = SwagCategory
            ammoCrate.physicsBody?.collisionBitMask = FloorCategory | SwagCategory
            
            
            
            ammoCrate.name = "ammoCrate"
            ammoCrate.position = CGPoint(x: Int(arc4random() % UInt32(self.frame.size.width - ammoCrate.size.width/2)) - Int(self.frame.size.width/2.0), y: Int(self.frame.height/2 + 10))
            
            self.addChild(ammoCrate)
            
        }
        
        let seq = SKAction.sequence([delay,spawn])
        self.run(SKAction.repeatForever(seq) , withKey: "spawning ammo")
        
        
    }
    
    
    
    
    
    
    
    func spawnEnemies(){
        
        let delay = SKAction.wait(forDuration: 3, withRange: 2)
        
        let spawn = SKAction.run {
            
            let enemy = Enemy()
            
       
            enemy.position = CGPoint(x: Int(arc4random() % UInt32(self.frame.size.width - enemy.size.width/2)) - Int(self.frame.size.width/2.0), y: Int(self.frame.height/2 + 10))
          
            enemy.zPosition = 1
  
            enemy.rocketBoost?.position = CGPoint(x: enemy.position.x, y: enemy.position.y - 0.8*enemy.size.height/2)
            enemy.rocketBoost?.zRotation = 2*CGFloat(M_PI_2)
            enemy.rocketBoost?.zPosition = -1
            
            
            
            self.addChild(enemy.rocketBoost!)
            self.addChild(enemy)
            
            
            
            
            let pinJoint = SKPhysicsJointFixed.joint(withBodyA: enemy.physicsBody!, bodyB: (enemy.rocketBoost?.physicsBody)!, anchor: (enemy.rocketBoost?.position)!)
            self.physicsWorld.add(pinJoint)
            
            
            
            self.enemies?.add(enemy)
            
        }
        
        let seq = SKAction.sequence([delay,spawn])
        self.run(SKAction.repeatForever(seq) , withKey: "spawning enemies")

    }
    
    
    func shoot(){
        let bullet = SKSpriteNode(color: SKColor.red, size: CGSize(width: 5, height: 5))
        bullet.name = "bullet"
        bullet.physicsBody = SKPhysicsBody(rectangleOf: bullet.size)
        bullet.physicsBody?.allowsRotation = false
        bullet.physicsBody?.affectedByGravity = false
        bullet.physicsBody?.velocity = CGVector(dx: 0, dy: 500)
        bullet.physicsBody?.categoryBitMask = BulletCategory
        bullet.physicsBody?.contactTestBitMask = EnemyCategory
        bullet.physicsBody?.collisionBitMask = 0
        
        
        bullet.position = CGPoint(x:(swag?.position.x)!, y: (swag?.position.y)! + (swag?.size.height)!/2)
        
        
        self.addChild(bullet)
    }

    
    
    func gameOver(){
        for enemy in enemies! {
            (enemy as? Enemy)?.rocketBoost?.removeFromParent()
            (enemy as? Enemy)?.removeFromParent()
        }
        swag?.removeFromParent()
		
		
	
        tryAgain = SKSpriteNode(color: SKColor.red, size: CGSize(width: 200, height: 100))
        tryAgain?.position = CGPoint(x: 0, y: 0)
        tryAgain?.name = "tryAgain"
        self.addChild(tryAgain!)
		let yourScore = SKLabelNode(text: "Your Score: "+(score?.description)!)
		yourScore.position = CGPoint(x: 0, y: (tryAgain?.position.y)! + (tryAgain?.size.height)!/2)
        let tryAgainLabel = SKLabelNode(text: "Try Again?")
        tryAgainLabel.name = "tryAgainLabel"
        
        tryAgainLabel.fontName = "AppleSDGothicNeo-Bold"
        
        tryAgainLabel.position = CGPoint(x: 0, y: 0)
        self.addChild(tryAgainLabel)
        self.removeAllActions()

    }
    
    func resetScene(){
        tryAgain?.removeFromParent()
        self.loadSceneNodes()
        self.childNode(withName: "tryAgainLabel")?.removeFromParent()
        healthBar?.color = SKColor.green
        
    }
    


}
